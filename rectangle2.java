package rectangle;

public class rectangle2 {
    private double width;
    private double height;

    // Constructor
    public rectangle2(double width, double height) {
        this.width = width;
        this.height = height;
    }

    // Method to calculate area
    public double calArea() {
        return width * height;
    }

    // Method to calculate perimeter
    public double calPerimeter() {
        return 2 * (width + height);
    }

    // Getter method for width
    public double getWidth() {
        return width;
    }

    // Setter method for width
    public void setWidth(double width) {
        this.width = width;
    }

    // Getter method for height
    public double getHeight() {
        return height;
    }

    // Setter method for height
    public void setHeight(double height) {
        this.height = height;
    }

    // Main method for testing
    public static void main(String[] args) {
        // Creating a Rectangle2 object with width 5 and height 10
        rectangle2 myRectangle = new rectangle2(5, 10);

        // Calculating and displaying area and perimeter using getter methods
        System.out.println("Area: " + myRectangle.calArea());
        System.out.println("Perimeter: " + myRectangle.calPerimeter());

        // Changing the width and height values using setter methods
        myRectangle.setWidth(8);
        myRectangle.setHeight(12);

        // Calculating and displaying updated area and perimeter using getter methods
        System.out.println("Updated Area: " + myRectangle.calArea());
        System.out.println("Updated Perimeter: " + myRectangle.calPerimeter());
    }
}

