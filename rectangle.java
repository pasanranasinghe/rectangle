package rectangle;

public class rectangle {
    private double width;
    private double height;

    // Constructor to initialize width and height
    public rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    // Method to calculate area of the rectangle
    public double calcArea() {
        return width * height;
    }

    // Method to calculate perimeter of the rectangle
    public double calcPerimeter() {
        return 2 * (width + height);
    }

    // Getter and setter methods for width attribute
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    // Getter and setter methods for height attribute
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public static void main(String[] args) {
        // Example usage of the Rectangle class
        rectangle rectangle = new rectangle(5.0, 10.0);
        System.out.println("Width: " + rectangle.getWidth());
        System.out.println("Height: " + rectangle.getHeight());
        System.out.println("Area: " + rectangle.calcArea());
        System.out.println("Perimeter: " + rectangle.calcPerimeter());
    }
}
